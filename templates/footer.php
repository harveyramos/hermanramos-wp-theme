<footer class="content-info" role="contentinfo">
  <div class="container">
      <div class="row">
          <?php if ( !is_front_page() ) dynamic_sidebar('sidebar-footer'); ?>
      </div>
      <div class="row text-center">
          &copy; <?= date("Y"); ?> Herman Ramos. All rights reserved. Design by <a href="http://littlerabbitstudios.com" target="_blank">Little Rabbit Studios</a>.
      </div>
  </div>
</footer>
