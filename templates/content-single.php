<?php while ( have_posts() ) : the_post(); ?>
    <article <?php post_class(); ?>>
        <header>
            <h1 class="entry-title"><?php the_title(); ?></h1>
            <?php get_template_part( 'templates/entry-meta' ); ?>
        </header>
        <div class="entry-content">
            <?php the_content(); ?>
        </div>
        <footer>
            <?php wp_link_pages( [
                'before' => '<nav class="page-nav"><ul class="pagination"><li><span>' . __( 'Pages:', 'sage' ) . '</span></li><li>',
                'after'  => '</li></ul></nav>',
                'link_before'      => '<span>',
                'link_after'       => '</span>',
                'next_or_number'   => 'number',
                'separator'        => '</li> <li>',
                'pagelink'         => '%'
            ] ); ?>
        </footer>
        <nav class="navigation posts-navigation">
            <ul class="pager">
                <?php previous_post_link( '<li class="previous">%link</li>', '<i class="fa fa-arrow-circle-left"></i> %title' ); ?>
                <?php next_post_link( '<li class="next">%link</li>', '%title <i class="fa fa-arrow-circle-right"></i>' ); ?>
            </ul>
        </nav>
        <?php comments_template( '/templates/comments.php' ); ?>
    </article>
<?php endwhile; ?>