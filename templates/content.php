<article <?php post_class('row'); ?>>
    <?php if ( '' != get_the_post_thumbnail() ) : $summaryColClass = 'col-lg-9 col-md-8 col-sm-6'; ?>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumbnail', [ 'class' => 'img-responsive featured'] ); ?></a>
        </div>

    <?php else : $summaryColClass = 'col-md-12'; endif; ?>

    <div class="entry-summary <?= $summaryColClass; ?>">
        <header>
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <?php get_template_part( 'templates/entry-meta' ); ?>
        </header>
        <?php the_excerpt(); ?>
        <footer class="entry-footer">
            <?php _e( Roots\Sage\Extras\hr_get_post_categories() ); ?>
        </footer>
    </div>
</article>
