<article <?php post_class('col-md-6'); ?>>
    <?php if ( '' != get_the_post_thumbnail() ) : ?>
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'grid', [ 'class' => 'img-responsive featured' ] ); ?></a>
    <?php endif; ?>
    <div class="entry-summary">
        <header>
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <?php get_template_part( 'templates/entry-meta' ); ?>
        </header>
        <?php the_excerpt(); ?>
        <footer class="entry-footer">
            <?php _e( Roots\Sage\Extras\hr_get_post_categories() ); ?>
        </footer>
    </div>
</article>
