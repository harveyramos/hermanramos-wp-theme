<header class="banner" role="banner">
    <div class="container">
        <nav class="navbar navbar-default navbar-fixed-top <?= ( is_front_page() ? "transparent":""); ?>">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/images/logo_herman-ramos.png" class="logo" alt="<?php bloginfo( 'name' ); ?>">
                    <a href="<?= esc_url( home_url( '/' ) ); ?>" class="navbar-brand"><?php bloginfo( 'name' ); ?></a>
                </div>
                <div id="navbar-right" class="navbar-collapse navbar-right collapse">
                    <?php
                    if ( has_nav_menu( 'secondary_navigation' ) ) :
                        wp_nav_menu( [ 'theme_location' => 'secondary_navigation', 'menu_class' => 'nav navbar-nav' ] );
                    endif;
                    ?>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <?php
                    if ( has_nav_menu( 'primary_navigation' ) ) :
                        wp_nav_menu( [ 'theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav' ] );
                    endif;
                    ?>
                </div>
                <!--/.nav-collapse -->
            </div>
        </nav>
    </div>
</header>
