<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Config;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Config\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Output the categories for a given post.
 * Taken from Genesis
 *
 * @param $args
 * @return mixed|void
 */
function hr_get_post_categories( $args = array() ) {
    $args = array_merge(
        [
            'after'  => '',
            'before' => __( 'Filed Under: ', 'sage' ),
            'sep'    => ' ',
        ],
        $args
    );

    $cats = get_the_category_list( trim( $args['sep'] ) . ' ' );
    //* Do nothing if no tags
    if ( ! $cats ) return '';

    return '<span class="categories">' . $args['before'] . $cats . $args['after'] . '</span>';
}