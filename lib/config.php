<?php

namespace Roots\Sage\Config;

use Roots\Sage\ConditionalTagCheck;

/**
 * Enable theme features
 */
add_theme_support( 'soil-clean-up' );           // Enable clean up from Soil
add_theme_support( 'soil-relative-urls' );      // Enable relative URLs from Soil
add_theme_support( 'se-nav-walker' );           // Enable cleaner nav walker from Soil
add_theme_support( 'soil-nice-search' );        // Enable nice search from Soil
add_theme_support( 'soil-js-to-footer' );       // Move all JS calls to the footer
add_theme_support('soil-jquery-cdn');        // Enable to load jQuery from the Google CDN
// add_theme_support('soil-google-analytics', 'UA-XXXXX-Y', 'wp_head');
// add_theme_support('soil-nav-walker');        // Enable cleaner nav walker from Soil

/**
 * Configuration values
 */
if (!defined('GOOGLE_ANALYTICS_ID')) {
    // Format: UA-XXXXX-Y (Note: Universal Analytics only)
    define('GOOGLE_ANALYTICS_ID', 'UA-41433635-1');
}
if (!defined('WP_ENV')) {
  // Fallback if WP_ENV isn't defined in your WordPress config
  // Used in lib/assets.php to check for 'development' or 'production'
  define('WP_ENV', 'production');
}

if (!defined('DIST_DIR')) {
  // Path to the build directory for front-end assets
  define('DIST_DIR', '/dist/');
}

/**
 * Define values for EDD theme update
 * Prefix is EDD_TU for Easy Digital Downloads Theme Update
 */
if (!defined('EDD_TU_REMOTE_API_URL'))
    define('EDD_TU_REMOTE_API_URL', 'https://littlerabbitstudios.com');

if (!defined('EDD_TU_ITEM_NAME'))
    define('EDD_TU_ITEM_NAME', 'Herman Ramos');

if (!defined('EDD_TU_THEME_SLUG'))
    define('EDD_TU_THEME_SLUG', 'hermanramos');

if (!defined('EDD_TU_VERSION'))
    define('EDD_TU_VERSION', '0.5.3');

if (!defined('EDD_TU_AUTHOR'))
    define('EDD_TU_AUTHOR', 'Harvey Ramos');

if (!defined('EDD_TU_DOWNLOAD_ID'))
    define('EDD_TU_DOWNLOAD_ID', '');

if (!defined('EDD_TU_RENEW_URL'))
    define('EDD_TU_RENEW_URL', '');

/**
 * Define which pages shouldn't have the sidebar
 */
function display_sidebar() {
  static $display;

  if (!isset($display)) {
    $conditionalCheck = new ConditionalTagCheck(
      /**
       * Any of these conditional tags that return true won't show the sidebar.
       * You can also specify your own custom function as long as it returns a boolean.
       *
       * To use a function that accepts arguments, use an array instead of just the function name as a string.
       *
       * Examples:
       *
       * 'is_single'
       * 'is_archive'
       * ['is_page', 'about-me']
       * ['is_tax', ['flavor', 'mild']]
       * ['is_page_template', 'about.php']
       * ['is_post_type_archive', ['foo', 'bar', 'baz']]
       *
       */
      [
        'is_404',
        'is_front_page',
        ['is_page_template', 'template-custom.php']
      ]
    );

    $display = apply_filters('sage/display_sidebar', $conditionalCheck->result);
  }

  return $display;
}
