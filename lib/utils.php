<?php

namespace Roots\Sage\Utils;

use Roots\Sage\Wrapper;

/**
 * Tell WordPress to use searchform.php from the templates/ directory
 */
function get_search_form() {
  $form = '';
  locate_template('/templates/searchform.php', true, false);
  return $form;
}
add_filter('get_search_form', __NAMESPACE__ . '\\get_search_form');

/**
 * Bug testing only. Not to be used on a production site!!
 */
function roots_wrap_info() {
    $format = '<h6>The %s template being used is: %s</h6>';
    // $main   = SageWrapping::$main_template;
    $main   = Wrapper\template_path();
    global $template;

    printf($format, 'Main', $main);
    printf($format, 'Base', $template);
    echo "<h6>is_single:".is_single()." is_page:".is_page()." is_archive:".is_archive()." is_category:".is_category()." ";
    echo "is_front_page:".is_front_page()." is_home:".is_home()."</h6>";
    echo "<h6>get_post_type:".get_post_type()." get_post_format:".get_post_format()."</h6>";
}
if (WP_ENV === 'development') {
    add_action('wp_footer', __NAMESPACE__ . '\\roots_wrap_info');
}