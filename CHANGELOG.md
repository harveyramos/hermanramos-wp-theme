### HEAD

### 0.5.3
* Fixed background image not fitting in mobile browser window
* Update Bootstrap to 3.3.5

### 0.5.2
* Added Google Analytics

### 0.5.1
* Removed extra padding on body

### 0.5.0
* Added spacing on top navigation
* Added secondary menu
* Added support for SageExtras Nav Walker (https://github.com/storm2k/sagextras)

### 0.4.0
* More media query and css tweaks
* removed pagepiling.js

### 0.3.0
* Media Query tweaks for mobile views
* Added EDD Updater

### 0.2.0
* Added fitvids JS for responsive videos.
* CSS Tweaks. Calendar, widgets, twitter widgets, footer widgets as columns

### 0.1.0
* fixed images breaking responsiveness
* Fix admin bar messing up layout
* Apparently, if I declare one path I must declare them all
* Updated Screenshot
* Added Gulp-Zip
* CSS Tweaks
* Added debugging
* Main nav transparent on homepage, goes solid on scroll with css transition
* Styled text on homepage
* Pagination styling.
* moved byline onto one line
* Added Google fonts. Open Sans as heading & nav, Vollkorn as body.
* Restored padding at top of page when not on home (pagepiling).
* Added background video mp4, and placeholder png
* Formatted the footer
* Create and style the top nav menu
* Set Roots/Soil plugin theme options
* Added devURL for BrowserSync
* fix image height on WP class .alignnone
* Update imagemin-pngcrush package version number

### 0.0.0
* Add search templates ([#1459](https://github.com/roots/sage/issues/1459))
* From Sage 8.2.1
