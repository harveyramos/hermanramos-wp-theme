<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php
$index = 1;
// Top post of the page
while ( have_posts() && $index == 1 ) : the_post();
    // first post, main featured image
    $contentType = get_post_type() != 'post' ? get_post_type() : get_post_format();
    $contentType = $contentType ? $contentType.'-featured': 'featured';
    get_template_part('templates/content',  $contentType);
    $index++;
endwhile;

// Grid of 4 posts
while ( have_posts() && $index <= 5 ) : the_post();
    // check if we need to open or close the row. Check If Even
    if ( $index % 2 == 0 )
        echo '<div class="row">';
    $contentType = get_post_type() != 'post' ? get_post_type() : get_post_format();
    $contentType = $contentType ? $contentType.'-grid': 'grid';
    get_template_part('templates/content',  $contentType);
    if ( $index % 2 != 0 )
        echo '</div>';
    $index++;
endwhile;

// Final 5 as a list
while ( have_posts() ) : the_post();
    $contentType = get_post_type() != 'post' ? get_post_type() : get_post_format();
    get_template_part('templates/content',  $contentType);
endwhile;
?>

<?php the_posts_navigation(); ?>
