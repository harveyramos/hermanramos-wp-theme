<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;

?>

<?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
    do_action( 'get_header' );
    get_template_part( 'templates/header' );
    ?>

    <video autoplay loop preload="none" poster="<?= get_stylesheet_directory_uri(); ?>/dist/images/bg_home.png" id="bgvid">
        <source src="<?= get_stylesheet_directory_uri(); ?>/dist/images/bg_video_herman-ramos.mp4" type="video/mp4">
    </video>

    <?php include Wrapper\template_path(); ?>

    <!--<div id="pagepiling">
        <div class="fl-row">
            <div class="container">
                <div class="intro">
                    <h1>Herman Ramos</h1>
                    <p>Dancer. Choreographer. Person.<br /><em>not always in that order...</em></p>
                    <p><?php /*bloginfo( 'description' ); */?></p>
                </div>
            </div>
        </div>
    </div>-->

    <?php get_template_part( 'templates/footer' ); ?>

    <?php wp_footer(); ?>

  </body>
</html>
